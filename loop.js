
function change_backgroud() {
    var divs = document.getElementsByTagName("div");
    for (var i = 0; i < divs.length; i++) {
        // Vị trí chẵn => màu đỏ
        if ((i + 1) % 2 == 0) {
            divs[i].style.background = "red";
        }
        else { // Vị trí lẽ => màu xanh
            divs[i].style.background = "blue";
        }
    }
}

function tinh_tong(n) {
    var tong = 0;
    var index = 0;

    while (index <= n) {
        // Nếu số chẵn thì cộng vào
        if (index % 2 == 0) {
            tong += index;
        }

        // tăng biến đếm
        index++;
    }

    document.write("Tổng số chẵn từ 1 tới " + n + " là: " + tong + "<br/>");
}